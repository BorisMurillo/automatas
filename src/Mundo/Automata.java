/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mundo;

/**
 *
 * @author Boris M
 */
public class Automata {
    
  private int cont=0;  
  private char palabra[]; 
  boolean aceptado=false;
  
  public Automata(String pal){
  palabra=pal.toCharArray();
  estadoInicio(palabra);
  }
  
  public boolean getResultado(){
  return aceptado;
  }
  
  
  private void estadoInicio(char [] pal){
  if(pal[cont]== ' '){
      aceptado=true;
      return;
  }
  else if(pal.length<4){ //la palabra es descartada al tener una longitud menor a 4
  return;
  }
  else if(pal[cont]=='a'){
      cont++;
      EstadoQ1(pal);
  }
    
  else if(pal[cont]=='b'){
      cont++;
      EstadoQ2(pal);
  }
  
  return; //la palabra es descartada al no comenzar con "a" ó "b"
  
  }
  
  private void EstadoQ1(char [] pal){
 
      if(cont<pal.length){
  
           if(pal[cont]=='a'){
           cont++;
           EstadoQ8(pal);
        }

        else if(pal[cont]=='b'){
            cont++;
            EstadoQ4(pal);
        }
      }
  return;
  
  }
  
  
  private void EstadoQ2(char [] pal){
  
      if(cont<pal.length){

            if(pal[cont]=='a'){
               cont++;
               EstadoQ4(pal);
            }

            else if(pal[cont]=='b'){
                cont++;
                EstadoQ3(pal);
            }
      }
  return;
      
  }
  
  
  private void EstadoQ3(char [] pal){
  
      if(cont<pal.length){

        if(pal[cont]=='a'){
        cont++;
        EstadoQ5(pal);
        }
        else if(pal[cont]=='b'){
             cont++;
             EstadoQ2(pal);
         }
        
      }
  return;
      
  }
  
  
  private void EstadoQ4(char [] pal){
  
      if(cont<pal.length){
 
        if(pal[cont]=='a'){
           cont++;
           EstadoQ7(pal);
        }

        else if(pal[cont]=='b'){
            cont++;
            EstadoQ5(pal);
        }
     }
  return;
      
  }
  
  
  private void EstadoQ5(char [] pal){
 
  if(cont<pal.length){
 
        if(pal[cont]=='a'){
           cont++;
           EstadoQ6(pal);
        }

        else if(pal[cont]=='b'){
            cont++;
            EstadoQ4(pal);
        }
   }
  return;
      
  }
  
  
 private void EstadoQ6(char [] pal){ //estado de aceptación
     
     if(cont==pal.length){
          aceptado=true;
          return;
     }
     
     else  if(cont<pal.length){
      
        if(pal[cont]=='a'){
                 cont++;
                 EstadoQ5(pal);    
        }

        else if(pal[cont]=='b'){
            cont++;
            EstadoQ7(pal);
        }
     }  
  return;
      
  }
 
  private void EstadoQ7(char [] pal){
 
  if(cont<pal.length){
 
        if(pal[cont]=='a'){
           cont++;
           EstadoQ4(pal);
        }

        else if(pal[cont]=='b'){
            cont++;
            EstadoQ6(pal);
        }
   }
  return;
      
  }
  
  
   private void EstadoQ8(char [] pal){
 
  if(cont<pal.length){
 
        if(pal[cont]=='a'){
           cont++;
           EstadoQ1(pal);
        }

        else if(pal[cont]=='b'){
            cont++;
            EstadoQ7(pal);
        }
   }
  return;
      
  }
  
  
}
