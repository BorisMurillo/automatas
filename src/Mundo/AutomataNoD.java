/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mundo;

import ufps.util.colecciones_seed.ArbolBinario;
import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author Boris M
 */
public class AutomataNoD {
    
    private boolean aceptado1=false;
    private Pila pila=new Pila();
    
    public AutomataNoD(String pal){
        if(pal.charAt(0)=='a') //solo inicia si el primer simbolo es "a"
            correr(pal.toCharArray());
        return;
    }
    
    public boolean getAceptado(){
    return aceptado1;
    }
    
    private void correr(char[] pal){
    if(pila.esVacia())
        pila.apilar(pal[0]);
    
        for(int i=1;i<pal.length;i++){
            
            if(pal[i]!='a' && pal[i]!='b')// comprueba que no llegue un simbolo diferente de 'a' ó 'b'
                return;
            else{
                if(pila.getTope().equals('a'))
                {
                    pila.apilar(pal[i]);
                    if(pila.getTamanio()>2 && pal[i]=='b') //si en la pila se encuentra "aa" u otra mayor cantidad-                                                        //y al venir "b"-
                        return;                    //- de a's concatenadas y llega una "b" la palabra es rechazada

                }
                else if(pila.getTope().equals('b'))
                {
                if(pal[i]=='a')//rechaza palabras que contengan "b" seguida de "a"
                    return;
                else
                    pila.apilar(pal[i]);
                }
            }
        }
        aceptado1=true;
        
    
        
    }
  
   
}
