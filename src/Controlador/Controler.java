/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Mundo.Automata;
import Vista.Vista;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Boris M
 */
public class Controler implements ActionListener{
    Vista v;
    Automata aut;
    
    public Controler(){
    v=new Vista();
    ActionListener(this);
    }

     public void ActionListener(ActionListener c) {
  
       v.btnIngresar.addActionListener(c);  
         
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      
        if(e.getSource().equals(v.btnIngresar)){
            String pal=v.txtPalabra.getText();
            if(pal.equals("")){
                aut=new Automata(" "); }
            else
                aut=new Automata(pal); 
            
            if(aut.getResultado()){
                v.txtResultado.setForeground(Color.green);
                v.txtResultado.setText("La Palabra Es Aceptada");
            
            }
            else {
                v.txtResultado.setText("La Palabra No Es Aceptada");
                v.txtResultado.setForeground(Color.red);
            }
        }
        
    }
    
    public static void main(String[] args) {
    Controler c=new Controler();
    }
    
}
