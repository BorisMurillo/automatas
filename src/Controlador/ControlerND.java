/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Mundo.Automata;
import Mundo.AutomataNoD;
import Vista.Vista;
import Vista.VistaND;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Boris M
 */
public class ControlerND implements ActionListener{
    VistaND v;
    AutomataNoD aut;
    
    public ControlerND(){
    v=new VistaND();
    ActionListener(this);
    }

     public void ActionListener(ActionListener c) {
  
       v.btnIngresar.addActionListener(c);  
         
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      
        if(e.getSource().equals(v.btnIngresar)){
            String pal=v.txtPalabra.getText();
            if(pal.equals("")){
                aut=new AutomataNoD(" "); }
            else
                aut=new AutomataNoD(pal); 
            
            if(aut.getAceptado()){
                v.txtResultado.setForeground(Color.green);
                v.txtResultado.setText("La Palabra Es Aceptada");
            
            }
            else {
                v.txtResultado.setText("La Palabra No Es Aceptada");
                v.txtResultado.setForeground(Color.red);
            }
        }
        
    }
    
    public static void main(String[] args) {
    ControlerND c=new ControlerND();
    }
    
}
